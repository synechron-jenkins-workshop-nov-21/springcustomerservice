## Instructions

# Deployment on VM
* Pre-req JDK and Maven should be installed, if not follow the below steps
    * sudo su
    * apt-get update
    * apt-get install default-jdk maven
* Let's create the jar package
    * mvn package
* Once you got the jar created, you can run the app as a java process
    * java -jar target/your_jar_file_name
* Now hit the vm on your respective service port, things will work perfectly fine for you.
    * vmip:6060 (in this case)
* Note: To start java process in background
    * nohup java -jar target/your_jar_file_name &

# Deployment in the form of Docker Contianer
* Pre-req JDK and Maven should be installed, if not follow the below steps
    * sudo su
    * apt-get update
    * apt-get install default-jdk maven
* Let's create the jar package
    * mvn package
* Now, you have the compiled the jar with you, let's create an Docker Image with that. Ideally, our image for this microservice should have - jdk, and your jar file, so below is the how our dockerfile should look like
    ```
      FROM openjdk
      COPY source_of_jar destination_of_jar
      CMD ["java","-jar","destination_of_jar"]
    ```
* To create docker file, change image_name as per your need for other services
    * docker build -t customerservice .
* Once image got created, you can create a container as well (update image_name and ports as per your need for other services)
    * docker run -d -p 8082:6060 customerservice
